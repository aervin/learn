import sys
import time
import pandas as pd
from glob import glob
from openpyxl import load_workbook
file_name = input('请输入保存的文件名（不带后缀）： ')   # 获取自定义文件名
if not file_name:
    file_name = 'merge'
full_name = '%s.xlsx' % file_name   # 把文件名组装带上后缀
file = glob('*.xls?')     # 扫描当前符合要求的文件
if not file:
    print('当前没有可合并的文件'); time.sleep(6); sys.exit()
temp_data = []      # 临时存放的数据
for data in file:
    df = pd.read_excel(data)    # 循环读取扫描到的文件
    temp_data.append(df)        # 把文件的数据保存到临时列表中
dfn = pd.concat(temp_data)      # 多个DataFrame数据合并为一个
dfn.to_excel(full_name, sheet_name=file_name, index=False)   # 写入到新的excel表中
del temp_data
workbook = load_workbook(full_name)
worksheet = workbook[workbook.sheetnames[0]]
# 设置列宽, 列和列宽根据需求可修改
worksheet.column_dimensions['B'].width = 20.0
worksheet.column_dimensions['C'].width = 23.0
worksheet.column_dimensions['D'].width = 13.0
workbook.save(full_name)
