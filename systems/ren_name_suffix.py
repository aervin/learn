import sys
import os
import time
from glob import glob

src_name_suffix = ''
while not src_name_suffix:
    print('输入要替换的文件或目录的后缀部分（区分大小写）')
    src_name_suffix = input()
dst_name_suffix = ''
while not dst_name_suffix:
    dst_name_suffix = input('输入新的后缀（区分大小写）：')

start_time = time.time()
src_path = '*%s' % src_name_suffix
src_name = glob(src_path)
if not src_name:
    print('没有匹配项')
    time.sleep(4)
    sys.exit()
total = len(src_name)
for src in src_name:
    dst_name = src.replace(src_name_suffix, dst_name_suffix)
    os.rename(src, dst_name)
    print('替换 %s 为 %s' % (src, dst_name))
end_time = time.time()
print('Done.')

run_time = round((end_time-start_time), 3)
print('共替换%s个' % total)
print('本次任务耗时：%s(s)' % run_time)
time.sleep(10)
sys.exit()
