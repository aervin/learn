import datetime, time
import requests
import os
import re

start_time = time.time()

header = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.162 Safari/537.36',
    'Referer': 'http://news.iciba.com/views/dailysentence/daily.html'
}
cookie = {
    'Cookie': ''
}

begin = datetime.date(2020, 1, 1)
end = datetime.date(2020, 4, 14)
d = end
delta = datetime.timedelta(days=1)
while d >= begin:
    date = d
    urls = 'http://sentence.iciba.com/index.php?callback=jQuery190011827250184575044_1586843238138&c=dailysentence&m=getdetail&title={0}&_=1586843238166'
    response = requests.get(urls.format(date), headers=header)
    data = response.text
    content_data = re.search(r'(\"content\":).*?(\",\"note\")', data)
    if content_data != None:
        content = content_data.group()[11:-8].encode('utf-8').decode('unicode_escape').replace(u'\u2002', u' ').replace(u'\xa0', u' ')
    note_data = re.search(r'(\"note\":).*?(\",)', data)
    if note_data != None:
        note = note_data.group()[8:-2].encode('utf-8').decode('unicode_escape')
    picture2_data = re.search(r'(\"picture2\":).*?(.jpg\",)', data)
    if picture2_data != None:
        picture2 = picture2_data.group()[12:-2].replace('\\', '')
    cont = '[%s]\n%s\n%s\n\n' % (date, content, note)
    txt = open('iciba_everyday.txt', 'a')
    txt.write(cont)
    txt.close()
    print('获取[%s]内容完成.' % date)

    isExists = os.path.exists('images')
    if not isExists:
        os.mkdir('images')
    try:
        pic = requests.get(picture2, headers=header, timeout=15)
        img_path = 'images/%s.jpg' % date
        img = open(img_path, 'wb')
        img.write(pic.content)
        img.close()
        print('下载[%s]图片完成.' % date)
    except requests.exceptions.ConnectionError:
        print('图片无法下载')
    
    d -= delta

end_time = time.time()
print('Done')
run_time = round((end_time-start_time), 3)
print('本次任务耗时：%s(s)' % run_time)
