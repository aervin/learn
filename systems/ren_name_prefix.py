import sys, os, time
from glob import glob

src_name_prefix = ''
while not src_name_prefix:
    print('输入要替换的文件或目录的前缀部分（区分大小写）')
    src_name_prefix = input()
dst_name_prefix = ''
while not dst_name_prefix:
    dst_name_prefix = input('输入新的前缀（区分大小写）：')

start_time = time.time()
src_path = '%s*' % src_name_prefix
src_name = glob(src_path)
if not src_name:
    print('没有匹配项')
    time.sleep(4)
    sys.exit()
total = len(src_name)
for src in src_name:
    dst_name = src.replace(src_name_prefix, dst_name_prefix)
    os.rename(src, dst_name)
    print('替换 %s 为 %s' % (src, dst_name))
end_time = time.time()
print('Done...')

run_time = round((end_time-start_time), 3)
print('共替换%s个' % total)
print('本次任务耗时：%s(s)' % run_time)
time.sleep(10)
sys.exit()
