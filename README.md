Python 示例
=======

中文 | [English](README.en.md)

> python语言学习示例，仅供学习交流使用。

## 运行环境

Python3.8 +

多个第三方库

## 目前示例：
- Qzone 留言板数据蜘蛛
- Qzone 留言数据可视化
- 批量修改文件名或目录名前缀
- 批量修改文件名或目录名后缀
- 合并多个相同模板的Excel内容
- 金山词霸每日一句爬取和题图下载

## Python第三方库安装

使用 pip命令安装
```bash
pip install package_name
```

## 开源许可

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
